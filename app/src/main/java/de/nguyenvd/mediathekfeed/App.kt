package de.nguyenvd.mediathekfeed

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import de.nguyenvd.mediathekfeed.di.component.AppComponent
import de.nguyenvd.mediathekfeed.di.component.DaggerAppComponent
import javax.inject.Inject

class App : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    private lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder().application(this)
                .build()
        component.inject(this)
    }

    override fun activityInjector() = dispatchingAndroidInjector

}