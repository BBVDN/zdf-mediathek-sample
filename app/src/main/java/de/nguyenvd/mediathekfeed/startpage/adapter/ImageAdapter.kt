package de.nguyenvd.mediathekfeed.startpage.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.nguyenvd.mediathekfeed.R
import de.nguyenvd.mediathekfeed.data.model.MediaDetail
import de.nguyenvd.mediathekfeed.extensions.inflate
import de.nguyenvd.mediathekfeed.ui.BaseListAdapter
import de.nguyenvd.mediathekfeed.ui.BaseViewHolder
import kotlinx.android.synthetic.main.row_teaser.view.*

class ImageAdapter : BaseListAdapter<MediaDetail>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemView = parent.inflate(R.layout.row_teaser, false)
        return BaseViewHolder(itemView)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = list[position]

        Glide.with(holder.itemView.context)
                .load(item.url)
                .into(holder.itemView.image)
    }
}