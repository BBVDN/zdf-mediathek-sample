package de.nguyenvd.mediathekfeed.startpage.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.AndroidSupportInjection
import de.nguyenvd.mediathekfeed.R
import de.nguyenvd.mediathekfeed.data.Constants
import de.nguyenvd.mediathekfeed.data.model.BaseMediaModel
import de.nguyenvd.mediathekfeed.extensions.hide
import de.nguyenvd.mediathekfeed.extensions.injectViewModel
import de.nguyenvd.mediathekfeed.startpage.adapter.ImageAdapter
import de.nguyenvd.mediathekfeed.startpage.adapter.StageAdapter
import de.nguyenvd.mediathekfeed.startpage.data.FeedType
import de.nguyenvd.mediathekfeed.startpage.viewmodel.FeedViewModel
import de.nguyenvd.mediathekfeed.ui.BaseListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_feed.*
import javax.inject.Inject

class FeedFragment private constructor() : Fragment() {

    companion object {
        private val TAG = FeedFragment::getTag.toString()

        private const val IMAGE_WIDTH = 1280

        fun newInstance(feedType: FeedType): FeedFragment {
            val args = Bundle().apply {
                this.putSerializable(Constants.KEY_FEEDTYPE, feedType)
            }

            val fragment = FeedFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: FeedViewModel
    private val disposable = CompositeDisposable()

    private var feedType: FeedType = FeedType.UNKNOWN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)

        viewModel = injectViewModel(viewModelFactory)

        feedType = arguments?.get(Constants.KEY_FEEDTYPE) as FeedType
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.layoutManager = LinearLayoutManager(context)

        when (feedType) {
            FeedType.STAGE -> setupStages(StageAdapter())
            FeedType.TEASER -> setupTeaserImages(ImageAdapter())
            else -> Toast.makeText(context, getString(R.string.unknown_feed_type), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        disposable.clear()
    }

    private fun setupStages(adapter: StageAdapter) {
        recyclerView.adapter = adapter

        val stageObservable = viewModel.getStages()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = {
                            adapter.setupList(it)
                        },
                        onError = {
                            Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                        }
                )
        disposable.add(stageObservable)
    }

    private fun setupTeaserImages(adapter: ImageAdapter) {
        recyclerView.adapter = adapter

        val teaserObservable = viewModel.getTeaserImages(IMAGE_WIDTH)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = {
                            adapter.setupList(it)
                        },
                        onError = {
                            Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                        }
                )
        disposable.add(teaserObservable)
    }

    private inline fun <reified T : BaseMediaModel> BaseListAdapter<T>.setupList(list: List<T>) {
        progressBar.hide()

        this.setList(list)
    }
}