package de.nguyenvd.mediathekfeed.startpage.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import de.nguyenvd.mediathekfeed.startpage.ui.FeedFragment
import de.nguyenvd.mediathekfeed.startpage.data.FeedType

class FeedFragmentPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> FeedFragment.newInstance(FeedType.STAGE)
            1 -> FeedFragment.newInstance(FeedType.TEASER)
            else -> FeedFragment.newInstance(FeedType.UNKNOWN)
        }
    }
}