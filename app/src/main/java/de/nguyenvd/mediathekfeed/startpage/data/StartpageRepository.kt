package de.nguyenvd.mediathekfeed.startpage.data

import de.nguyenvd.mediathekfeed.data.api.ApiService
import de.nguyenvd.mediathekfeed.data.model.Media
import de.nguyenvd.mediathekfeed.data.model.MediaDetail
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StartpageRepository @Inject constructor(
        private val apiService: ApiService
) {

    companion object {
        private val TAG = StartpageRepository::class.java.simpleName
    }

    fun getStages(): Observable<Media> {
        return apiService.getStartResponse()
                .subscribeOn(Schedulers.io())
                .toObservable()
                .flatMap { Observable.fromIterable(it.stages) }
    }

    fun getTeaserImages(): Observable<MediaDetail> {
        return getStages()
                .subscribeOn(Schedulers.io())
                .flatMapIterable { it.mediaDetails.values }
    }
}