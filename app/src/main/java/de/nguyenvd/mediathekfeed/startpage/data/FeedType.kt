package de.nguyenvd.mediathekfeed.startpage.data

enum class FeedType {
    STAGE,
    TEASER,
    UNKNOWN
}