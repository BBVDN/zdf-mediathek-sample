package de.nguyenvd.mediathekfeed.startpage.viewmodel

import androidx.lifecycle.ViewModel
import de.nguyenvd.mediathekfeed.data.model.Media
import de.nguyenvd.mediathekfeed.data.model.MediaDetail
import de.nguyenvd.mediathekfeed.startpage.data.StartpageRepository
import io.reactivex.Single
import javax.inject.Inject

class FeedViewModel @Inject constructor(private val startpageRepo: StartpageRepository) : ViewModel() {

    fun getStages(): Single<List<Media>> {
        return startpageRepo.getStages()
                .toList()
    }

    fun getTeaserImages(width: Int): Single<List<MediaDetail>> {
        return startpageRepo.getTeaserImages()
                .filter { it.width == width }
                .toList()
    }
}