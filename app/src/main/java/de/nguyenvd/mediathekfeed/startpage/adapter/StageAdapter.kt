package de.nguyenvd.mediathekfeed.startpage.adapter

import android.view.ViewGroup
import de.nguyenvd.mediathekfeed.R
import de.nguyenvd.mediathekfeed.data.model.Media
import de.nguyenvd.mediathekfeed.extensions.inflate
import de.nguyenvd.mediathekfeed.ui.BaseListAdapter
import de.nguyenvd.mediathekfeed.ui.BaseViewHolder
import kotlinx.android.synthetic.main.row_stage.view.*

class StageAdapter : BaseListAdapter<Media>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflatedView = parent.inflate(R.layout.row_stage, false)
        return BaseViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = list[position]

        holder.itemView.headline.text = item.headline
        holder.itemView.title.text = item.title
        holder.itemView.description.text = item.description
    }
}