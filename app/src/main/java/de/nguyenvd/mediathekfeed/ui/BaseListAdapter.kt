package de.nguyenvd.mediathekfeed.ui

import androidx.recyclerview.widget.RecyclerView
import de.nguyenvd.mediathekfeed.data.model.BaseMediaModel

abstract class BaseListAdapter<T : BaseMediaModel> : RecyclerView.Adapter<BaseViewHolder>() {

    internal var list: MutableList<T> = mutableListOf()

    fun setList(list: List<T>) {
        this.list = list.toMutableList()
        notifyDataSetChanged()
    }
}