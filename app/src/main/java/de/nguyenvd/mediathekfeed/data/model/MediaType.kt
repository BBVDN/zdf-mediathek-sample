package de.nguyenvd.mediathekfeed.data.model

enum class MediaType(val typeString: String) {
    VIDEO("video")
}