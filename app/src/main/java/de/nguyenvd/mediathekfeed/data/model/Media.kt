package de.nguyenvd.mediathekfeed.data.model

import com.google.gson.annotations.SerializedName

data class Media(
        val id: String,
        val type: MediaType,
        val headline: String,
        @SerializedName("titel") val title: String,
        @SerializedName("beschreibung") val description: String,
        @SerializedName("teaserBild") val mediaDetails: Map<Int, MediaDetail>
) : BaseMediaModel()