package de.nguyenvd.mediathekfeed.data.model

data class MediaDetail(
        val url: String,
        val width: Int,
        val height: Int
) : BaseMediaModel()