package de.nguyenvd.mediathekfeed.data.api

import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("start-page")
    fun getStartResponse(): Single<Response>

    companion object {
        private const val BASE_URL: String = "https://zdf-cdn.live.cellular.de"
        private const val API_VERSION: String = "/mediathekV2/"
        const val API_URL = BASE_URL + API_VERSION
    }
}