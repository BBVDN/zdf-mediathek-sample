package de.nguyenvd.mediathekfeed.data.api

import com.google.gson.annotations.SerializedName
import de.nguyenvd.mediathekfeed.data.model.Media

data class Response(
        @SerializedName("stage") val stages: Collection<Media>
)