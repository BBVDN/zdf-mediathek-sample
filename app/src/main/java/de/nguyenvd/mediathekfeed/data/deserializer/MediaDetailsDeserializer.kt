package de.nguyenvd.mediathekfeed.data.deserializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import de.nguyenvd.mediathekfeed.data.model.MediaDetail
import java.lang.reflect.Type


class MediaDetailsDeserializer : JsonDeserializer<Map<Int, MediaDetail>> {

    override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
    ): Map<Int, MediaDetail>? {
        val jsonObject = json?.asJsonObject

        val details = HashMap<Int, MediaDetail>()

        jsonObject?.entrySet()?.let { entries ->
            for (entry in entries) {
                val img: MediaDetail? = context?.deserialize(entry.value, MediaDetail::class.java)

                img?.let {
                    details.put(entry.key.toInt(), it)
                }

            }
        }
        return details
    }
}