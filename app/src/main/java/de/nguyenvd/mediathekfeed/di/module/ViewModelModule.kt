package de.nguyenvd.mediathekfeed.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import de.nguyenvd.mediathekfeed.di.ViewModelFactory
import de.nguyenvd.mediathekfeed.di.ViewModelKey
import de.nguyenvd.mediathekfeed.startpage.viewmodel.FeedViewModel

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel::class)
    abstract fun bindFeedViewModel(viewModel: FeedViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
