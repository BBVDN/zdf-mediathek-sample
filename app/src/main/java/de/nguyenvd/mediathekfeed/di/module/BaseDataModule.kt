package de.nguyenvd.mediathekfeed.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import dagger.Module
import dagger.Provides
import de.nguyenvd.mediathekfeed.data.deserializer.MediaDetailsDeserializer
import de.nguyenvd.mediathekfeed.data.model.MediaDetail
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
class BaseDataModule {

    private val mediaDetailType = object : TypeToken<Map<Int, MediaDetail>>() {}.type

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder()
            .registerTypeAdapter(
                    mediaDetailType,
                    MediaDetailsDeserializer()
            )
            .setLenient()
            .create()

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .build()
}