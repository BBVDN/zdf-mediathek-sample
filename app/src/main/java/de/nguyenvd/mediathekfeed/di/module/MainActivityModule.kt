package de.nguyenvd.mediathekfeed.di.module


import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.nguyenvd.mediathekfeed.MainActivity

@Suppress("unused")
@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

}
