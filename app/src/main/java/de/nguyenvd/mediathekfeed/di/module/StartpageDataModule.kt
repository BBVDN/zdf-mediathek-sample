package de.nguyenvd.mediathekfeed.di.module

import dagger.Module
import dagger.Provides
import de.nguyenvd.mediathekfeed.data.api.ApiService
import de.nguyenvd.mediathekfeed.startpage.data.StartpageRepository
import javax.inject.Singleton

@Module(includes = [
    BaseDataModule::class,
    NetworkModule::class,
    ViewModelModule::class
])
class StartpageDataModule {

    @Provides
    @Singleton
    fun provideStartpageRepository(
            apiInterface: ApiService
    ): StartpageRepository = StartpageRepository(apiInterface)
}