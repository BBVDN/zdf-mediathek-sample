package de.nguyenvd.mediathekfeed.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.nguyenvd.mediathekfeed.startpage.ui.FeedFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeFeedFragment(): FeedFragment
}
